var hi = new Vivus('hi-there', {
	type: 'scenario-sync',
	duration: 7,
	forceRender: false,
	dashGap: 2,
	delay: 1,  
	start: 'manual',
}, function(){});

var crossSell = new Vivus('crossSell_svg', {
	type: 'delayed',
	duration: 40,
	forceRender: false,
	dashGap: 2,
	delay: 12,
	start: 'manual',

	pathTimingFunction: Vivus.EASE_IN,
}, function() {});

var searchSvg = new Vivus('search_svg', {
	type: 'scenario-sync',
	duration: 2,
	forceRender: false,
	dashGap: 2,
	start: 'manual'
}, function() {});


var adv = new Vivus('Adversting_svg', {
	type: 'scenario-sync',
	duration: 7,
	forceRender: false,
	dashGap: 2,
	start: 'manual'
}, function() {});


$(document).ready(function(){
	new manualStart($("#hi-there"),hi);
	new manualStart($("#search_svg"),searchSvg);
	new manualStart($("#Adversting_svg"),adv);
	new manualStart($("#crossSell_svg"),crossSell);

})

function manualStart(element, vivus){
	var wH = $(window).height() - ($(window).height()/3);
	var svgOff = element.offset().top;
	var svgStatus = 0;
	var wOff = 0;
	var t = this;
	$(window).scroll(function(t){		
		wOff = $(window).scrollTop();
		if(Math.round(wH + wOff) > svgOff && svgStatus == 0){

			svgStatus = 1;
			element.css('visibility','visible');
			vivus.reset().play();
		}
	});
}