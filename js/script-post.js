$(document).ready(function(){

        var widthofscreen = jQuery(window).width();

        	if (widthofscreen > 990) { 
				//EqualHeights triggering via JS for viewports > 990px

			    $('.news-post-head-half').setAllToMaxHeight();	
        	}

	   		else {
	   			//If EqualHeights are not being triggered, then Height is not fixed value. So we are removing '.valign' (vertical align) on all COL-* Elements (Bootstrap Columns) when viewed on viewports width < 990px
				$( "*[class^='col-md-']").find('.valign').removeClass('valign');
			}


    });
$.fn.setAllToMaxHeight = function(){
	return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
}