var uProfiling = new Vivus('user_profiling_svg', {
	type: 'scenario-sync',
	duration: 10,
	forceRender: false,
	dashGap: 2,
	start: 'manual'
}, function(){});

$(document).ready(function(){
	new manualStart($("#user_profiling_svg"),uProfiling);
})

function manualStart(element, vivus){
	var wH = $(window).height() - ($(window).height()/3);
	var svgOff = element.offset().top;
	var svgStatus = 0;
	var wOff = 0;
	var t = this;
	$(window).scroll(function(t){		
		wOff = $(window).scrollTop();
		if(Math.round(wH + wOff) > svgOff && svgStatus == 0){

			svgStatus = 1;
			element.css('visibility','visible');
			vivus.reset().play();
		}
	});
}