var websiteSvg = new Vivus('website_svg', {
	type: 'scenario-sync',
	duration: 7,
	forceRender: false,
	dashGap: 2,
	start: 'manual'
}, function() {});

var app = new Vivus('App_svg', {
	type: 'scenario-sync',
	duration: 7,
	forceRender: false,
	dashGap: 2,
	start: 'manual'
},  function() {});

var bi = new Vivus('bi-svg', {
	type: 'scenario-sync',
	duration: 7,	
	forceRender: false,
	dashGap: 2,
	start: 'manual'    
},  function(obj) {});


var dataPlatform = new Vivus('dataPlatform_svg', {
	type: 'scenario-sync',
	duration: 7,
	forceRender: false,
	dashGap: 2,
	delay: 1,
	start: 'manual',
  	pathTimingFunction: Vivus.EASE_IN,
}, function() {});

var iconEC = new Vivus('iconEasyconnect_svg', {
	type: 'scenario-sync',
	duration: 8,			  
	forceRender: false,
	dashGap: 2,
	start: 'manual' 
}, function() {});

var iconRES = new Vivus('iconResult_svg', {
	type: 'scenario-sync',
	duration: 8,	
	forceRender: false,
	dashGap: 2,
	start: 'manual'
}, function(obj) {});

var iBusiness = new Vivus('icon_business', {
	type: 'scenario-sync',
	duration: 8,
	forceRender: false,
	dashGap: 2,
	start: 'manual'
}, function() {});

$(document).ready(function(){
	new manualStart($("#website_svg"),websiteSvg);
	new manualStart($("#App_svg"),app);
	new manualStart($("#bi-svg"),bi);
	new manualStart($("#dataPlatform_svg"),dataPlatform);
	new manualStart($("#iconEasyconnect_svg"),iconEC);
	new manualStart($("#iconResult_svg"),iconRES);
	new manualStart($("#icon_business"),iBusiness);
})

function manualStart(element, vivus){
	var wH = $(window).height() - ($(window).height()/3);
	var svgOff = element.offset().top;
	var svgStatus = 0;
	var wOff = 0;
	var t = this;
	$(window).scroll(function(t){		
		wOff = $(window).scrollTop();
		if(Math.round(wH + wOff) > svgOff && svgStatus == 0){

			svgStatus = 1;
			element.css('visibility','visible');
			vivus.reset().play();
		}
	});
}